import os
import unittest

from flask import request, redirect
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app import blueprint
from app.main import create_app, db
from app.main.model import user, author, books, loanedbooks, notes, favorites, favoriteslist
import pytest

env = os.getenv('APP_ENV') or 'dev'
print(env)

app = create_app(env)
app.register_blueprint(blueprint)

app.app_context().push()

manager = Manager(app)

migrate = Migrate(app, db)

manager.add_command('db', MigrateCommand)

@manager.command
def run():
    app.run()


@manager.command
def test():
    """Runs the unit tests."""
    tests = unittest.TestLoader().discover('app/test', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1

@manager.command
def pytests():
    pytest_args = [
        'app/test'
    ]
    pytest.main(pytest_args)

@manager.command
def resetdb():
    db.session.commit()
    db.drop_all()
    db.create_all()
    db.session.commit()

if __name__ == '__main__':
    manager.run()
