CREATE TABLE users (
  userid serial primary key,
  firstname text,
  lastname text,
  email text,
  phone text,
  isadmin boolean
);

INSERT INTO users (firstname, lastname, email, phone, isadmin) VALUES ('Prakash', 'Somasundaram', 'sprakash1993@ymail.com', '2068229648', true);

INSERT INTO users (firstname, lastname, email, phone, isadmin) VALUES ('Bob', 'van', 'bob@gmail.com', '2068221234', false );