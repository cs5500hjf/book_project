import os
import logging
basedir = os.path.abspath(os.path.dirname(__file__))

#HEROKU_URL = 'postgres://kergkzwbcamkkm:9ad4d6c3b1224f3812e56602a4961c6a4eab37fd84feb75e5159dab3e86ea77a@ec2-54-243-137-182.compute-1.amazonaws.com:5432/d24jllna97u2nq'

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'my-secret-key'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'postgresql://postgres:postgres@localhost/postgres')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    ERROR_404_HELP = False
    MAIL_SERVER='smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'bookscatalogproject@gmail.com'
    MAIL_PASSWORD = 'Watermarke@401'

class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    DEBUG = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False


config_by_name = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)

key = Config.SECRET_KEY
