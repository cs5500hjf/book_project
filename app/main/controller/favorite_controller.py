from flask import request
from flask_restplus import Resource, fields, marshal

from ..util.dto import FavoriteDto
from ..util.dto import FavoriteListDto
from ..service.favorite_service import  get_books_by_favoriteid, get_favorites, create_a_favorites, delete_a_favorites, add_a_book_to_favoriteslist, delete_a_book_from_favoriteslist

api = FavoriteDto.favoriteapi
_favorite = FavoriteDto.favorite_get
_favoritePost = FavoriteDto.favorite_post
_favoritelist = FavoriteListDto.favoritelist

@api.route('')
class Favorite(Resource):
    @api.response(201, 'Favorite successfully created.')
    @api.doc('create_a_favorite')
    @api.expect(_favoritePost, validate=True)
    def post(self):
        data = marshal(request.json, _favoritePost)
        res, code = create_a_favorites(data=data)
        return marshal(res, _favorite), code

@api.route('/<favoriteid>')
@api.param('favoriteid', 'The Favorite identifier')
class DeleteFavorite(Resource):
    @api.response(200, 'Favorite successfully deleted.')
    @api.response(404, 'Favorite does not exist.')
    @api.doc('delete_a_favorites')
    def delete(self, favoriteid):
        return delete_a_favorites(favoriteid)
        
    @api.doc('get_favorites_by_id')
    @api.response(200, 'success')
    @api.response(404, 'favorite does not exist.')
    def get(self, favoriteid):
        res, code = get_favorites(favoriteid)
        if code == 404:
            return res, code
        return marshal(res, _favorite), code


@api.route('/<favoriteid>/book/<bookid>')
@api.param('favoriteid', 'The Favorite identifier')
@api.param('bookid', 'The Book identifier')
class FavoriteList(Resource):
    @api.response(201, 'Book successfully added.')
    @api.response(404, 'Book already exists in this list')
    @api.doc('add_a_book_to_favoriteslist')
    def post(self, favoriteid, bookid):
        return add_a_book_to_favoriteslist(favoriteid, bookid)
    
    @api.response(200, 'Book successfully deleted')
    @api.response(404, 'Book does not exist in this list.')
    @api.doc('delete_a_book_from_favoriteslist')
    def delete(self, favoriteid, bookid):
        return delete_a_book_from_favoriteslist(favoriteid, bookid)

@api.route('/<favoriteid>/book')
@api.param('favoriteid', 'The Favorite identifier')
class FavoriteBooks(Resource):
    @api.response(200, 'Books successfully loaded.')
    @api.response(404, 'No books exists in this list.')
    @api.doc('get_books_by_favoriteid')
    def get(self, favoriteid):
        res, code = get_books_by_favoriteid(favoriteid)
        if code == 404:
            return res, code
        return marshal(res, _favoritelist), code
    

