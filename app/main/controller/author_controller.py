from flask import request
from flask_restplus import Resource, marshal

from ..util.dto import AuthorsDto
from ..model.author import Author
from ..service.authors_service import save_new_author, get_author, get_authorid, get_all_authors, delete_author, update_author

api = AuthorsDto.authorapi
_author_get = AuthorsDto.authors_get
_author_post = AuthorsDto.authors_post

"""/authors"""
@api.route('')
class Authors(Resource):
    @api.doc('get_all_authors')
    @api.marshal_list_with(_author_get)
    def get(self):
     return get_all_authors()

    @api.response(201, 'Author successfully added.')
    @api.response(409, 'Author already exists.')
    @api.doc('add_new_author')
    @api.expect(_author_post, validate=True)
    def post(self):
      data = request.json
      res_data, status_code = save_new_author(data)
      if status_code == 201:
          return marshal(res_data, _author_get), status_code
      else:
          return res_data, status_code

"""/{authorid}"""
@api.route('/<authorid>')
@api.param('authorid', 'The Author identifier')
class GetAuthor(Resource):
    @api.doc('get_author_by_this_id')
    @api.marshal_with(_author_get)
    def get(self, authorid):
        author = get_author(authorid)
        if not author:
            api.abort(404, message='Author not found. Please verify the authorid')
        return author, 200

    @api.doc('update_an_author')
    @api.marshal_with(_author_get)
    @api.expect(_author_post, validate=True)
    def put(self, authorid):
        author = get_author(authorid)
        if not author:
            api.abort(404, message='Author not found. Please verify the authorid')
        data = request.json
        return update_author(authorid, data)

    @api.doc('delete_an_author')
    def delete(self, authorid):
        author = get_author(authorid)
        if not author:
            api.abort(404, message='Author not found. Please verify the authorid')
        return delete_author(authorid)



