from flask import request
from flask_restplus import Resource, fields, marshal

from ..util.dto import NotesDto
from ..service.notes_service import save_new_notes, get_a_note, get_all_notes, check_if_book_exists, update_a_note, delete_a_note

api = NotesDto.notesapi
_note_get = NotesDto.note_get
_note_post = NotesDto.note_post

@api.route('/<bookid>/note')
@api.param('bookid', 'The Book identifier')
class NotesList(Resource):
    @api.doc('list_of_all_notes')
    @api.marshal_list_with(_note_get)
    def get(self, bookid):
        if not check_if_book_exists(bookid):
            api.abort(404, message='Book not found. Please verify the bookid')
        notes = get_all_notes()
        if not notes:
            api.abort(404, message='No notes found for this book')
        else:
            return notes

    @api.response(201, 'Note successfully created.')
    @api.doc('create_new_note')
    @api.expect(_note_post, validate=True)
    def post(self, bookid):
        if not check_if_book_exists(bookid):
            api.abort(404, message='Book not found. Please verify the bookid')
        data = request.json
        note, status_code = save_new_notes(data, bookid)
        if status_code == 201:
            return marshal(note, _note_get), status_code
        else:
            response_object = {
                'status': 'failed',
                'message': 'Unable to create Note.'
            }
            return response_object, 500

@api.route('/<bookid>/note/<noteid>')
@api.param('bookid', 'The Book identifier')
@api.param('noteid', 'The Note identifier')
class Note(Resource):
    @api.doc('update_a_note')
    @api.expect(_note_post, validate=True)
    @api.marshal_with(_note_get)
    def put(self, bookid, noteid):
        if not check_if_book_exists(bookid):
            api.abort(404, message='Book not found. Please verify the bookid')
        data = request.json
        return update_a_note(data, noteid, bookid)

    @api.doc('delete_a_note')
    def delete(self, bookid, noteid):
        if not check_if_book_exists(bookid):
            api.abort(404, message='Book not found. Please verify the bookid')
        data = request.json
        return delete_a_note(noteid)

