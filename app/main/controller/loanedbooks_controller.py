from flask import request
from flask_restplus import Resource, fields

from ..util.dto import LoanedBooksDto
from ..service.loanedbooks_service import remind_all_users, send_reminder_to_user, get_books_loanedby_user, get_all_loan_records
from ..model.loanedbooks import LoanedBooks
from ..service.user_service import get_a_user

api = LoanedBooksDto.loanedbooksapi
_loan_get = LoanedBooksDto.loanedbooks_get
_loan_post = LoanedBooksDto.loanedbooks_post
_loan_put = LoanedBooksDto.loanedbooks_put

@api.route('/all')
class ReminderForAll(Resource):
    @api.doc('view_all_loan_records')
    @api.marshal_list_with(_loan_get)
    def get(self):
     return get_all_loan_records()


@api.route('/remindall')
class ReminderForAll(Resource):
    @api.doc('send_reminder_to_all_users')
    def get(self):
     return remind_all_users()


@api.route('/remind/<loanid>')
@api.param('loanid', 'The Loan identifier')
class Loan(Resource):
    @api.doc('send_reminder_to_user_for_book_return')
    def get(self, loanid):
      loan = LoanedBooks.query.filter_by(loanid=loanid).first()
      if not loan:
        api.abort(404, message='Loan not found. Please verify the loanid')
      else:
        return send_reminder_to_user(loanid)


@api.route('/<userid>')
@api.param('userid', 'The User identifier')
@api.response(200, 'loan records by user')
class BooksListLoanedByUser(Resource):
    @api.doc('get_all_loan_records_by_user')
    @api.marshal_list_with(_loan_get)
    def get(self, userid):
      user = get_a_user(userid)
      if not user:
        api.abort(404, message='User not found')
      else:
        return get_books_loanedby_user(userid)
