from flask import request
from flask_restplus import Resource, fields, reqparse, marshal
import datetime

from ..util.dto import BookDto
from ..util.dto import LoanedBooksDto
from ..service.loanedbooks_service import save_new_loaned_book
from ..service.book_service import get_all_books, get_a_book, save_new_book, get_all_loaned_books, update_book_isloaned, update_book_isreturned, delete_a_book, search_a_book, update_a_book
from ..service.user_service import get_a_user
from ..service.authors_service import get_author
from ..service.loanedbooks_service import set_the_return_date, delete_book_loans

bookapi = BookDto.bookapi
_book_put_post = BookDto.book_put_post
_book_get = BookDto.book_get
_loanedbooks_put = LoanedBooksDto.loanedbooks_put
_loanedbooks_post = LoanedBooksDto.loanedbooks_post
_loanedbooks_get = LoanedBooksDto.loanedbooks_get

@bookapi.route('')
class BooksList(Resource):
    @bookapi.doc('all_books')
    @bookapi.marshal_list_with(_book_get)
    def get(self):
        return get_all_books()

    @bookapi.response(201, 'Book successfully created.')
    @bookapi.response(409, 'Book already exists.')
    @bookapi.response(404, 'Author not found')
    @bookapi.doc('add_new_book')
    @bookapi.expect(_book_put_post, validate=True)
    def post(self):
        data = request.json
        userid= data['userid']
        user = get_a_user(userid)
        authorid = data['authorid']
        author = get_author(authorid)

        if not user or not user.isadmin:
          bookapi.abort(404, message='Admin not found. Please verify the userid')
        elif not author:
          bookapi.abort(404, message='Author not found. Please verify the authorid')
        else:
          res_data, status_code = save_new_book(data=data)
          if status_code == 201:
            return marshal(res_data, _book_get), status_code
          else:
            return res_data, status_code


@bookapi.route('/<bookid>')
@bookapi.param('bookid', 'The Book identifier')
@bookapi.response(404, 'Book not found.')
class Book(Resource):
    @bookapi.doc('get_a_book')
    @bookapi.marshal_with(_book_get)
    def get(self, bookid):
        book = get_a_book(bookid)
        if not book:
            bookapi.abort(404, message='Book not found.')
        else:
            return book

    @bookapi.doc('update_a_book')
    @bookapi.marshal_with(_book_get)
    @bookapi.expect(_book_put_post, validate=True)
    def put(self, bookid):
        book = get_a_book(bookid)
        if not book:
            bookapi.abort(404, message='Book not found.')
        else:
            data = request.json
            userid = data['userid']
            user = get_a_user(userid)
            authorid = data['authorid']
            author = get_author(authorid)

            if not user or not user.isadmin:
                bookapi.abort(404, message='Admin not found. Please verify the userid')
            elif not author:
                bookapi.abort(404, message='Author not found. Please verify the authorid')
            else:
                return update_a_book(bookid, data)

    @bookapi.doc('delete_a_book')
    @bookapi.marshal_with(_book_get)
    def delete(self, bookid):
        book = get_a_book(bookid)
        if not book:
            bookapi.abort(404, message='Book not found.')
        else:
            delete_book_loans(bookid)
            return delete_a_book(bookid)


@bookapi.route('/loaned')
@bookapi.response(200, 'Books Loaned')
class AllLoanedBooksList(Resource):
    @bookapi.doc('get_all_loaned_books')
    @bookapi.marshal_list_with(_book_get)
    def get(self):
      return get_all_loaned_books()


# @bookapi.route('/loaned/<userid>')
# @bookapi.param('userid', 'The User identifier')
# @bookapi.response(200, 'get all books loaned by this user')
# class AllLoanedBooksListByUser(Resource):
#     @bookapi.doc('get the book loaned by user')
#     @bookapi.marshal_with(_book, envelope='data')
#     def get(self, userid):
#       user = get_a_user(userid)
#       if not user:
#         bookapi.abort(404, message='User not found')
#       else:
#         return get_all_books_loanedby_user(userid)


@bookapi.route('/<bookid>/loaned/<userid>')
@bookapi.param('bookid', 'The Book identifier')
@bookapi.param('userid', 'The User identifier')
@bookapi.expect(_loanedbooks_post, validate=True)
@bookapi.response(200, 'loan this book to the user')
class LoanBookToUser(Resource):
    @bookapi.doc('loan_this_book_to_the_user')
    @bookapi.marshal_with(_loanedbooks_get)
    def post(self, bookid, userid):
      book = get_a_book(bookid)
      if book.isloaned==True:
        bookapi.abort(404, message='Book already loaned to someone')
      else:
        update_book_isloaned(bookid)
        data = request.json
        return save_new_loaned_book(data)


"""/{bookid}/returned/{loanid}"""
@bookapi.route('/<bookid>/returned/<loanid>')
@bookapi.param('bookid', 'The Book identifier')
@bookapi.param('loanid', 'The Loan identifier')
@bookapi.expect(_loanedbooks_put, validate=True)
@bookapi.response(200, 'book returned by the user')
class ReturnBook(Resource):
    @bookapi.doc('book_is_returned_by_user')
    @bookapi.marshal_with(_loanedbooks_put)
    def put(self, bookid, loanid):
      book = get_a_book(bookid)
      if book.isloaned==False:
        bookapi.abort(404, message='Book already returned')
      else:
        update_book_isreturned(bookid)
        data = request.json
        return set_the_return_date(data, loanid)


@bookapi.route('/search')
@bookapi.response(200, 'Search Successful')
@bookapi.response(404, 'Genre does not exist')
class BookSearch(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('startdate', type=str, help='Start date to search in yyyy-mm-dd format')
    parser.add_argument('enddate', type=str, help='End date to search in yyyy-mm-dd format')
    parser.add_argument('genre', type=str, help='Genre')
    parser.add_argument('authorid', type=int, help='Author Id')

    @bookapi.doc('search_a_book')
    @bookapi.expect(parser)
    def get(self):
        args = self.parser.parse_args()
        genres = ["sci-fi", "thriller", "action", "romance", "comedy"]
        if args.genre and not args.genre in genres:
            print(args.genre)
            bookapi.abort(404, message='Genre not found')
        bookslist = search_a_book(args)
        if bookslist:
            return marshal(bookslist, _book_get), 200
        else:
            return {
                'message': 'No Books found with the given search criteria.'
            }, 200
