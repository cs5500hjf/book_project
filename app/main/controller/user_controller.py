from flask import request
from flask_restplus import Resource, marshal

from ..util.dto import UserDto
from ..service.user_service import save_new_user, get_all_users, get_a_user

api = UserDto.userapi
_user_get = UserDto.user_get
_user_post = UserDto.user_post

@api.route('')
class UserList(Resource):
    @api.doc('list_of_all_users')
    @api.marshal_list_with(_user_get)
    def get(self):
        return get_all_users()

    @api.response(201, 'User successfully created.')
    @api.response(409, 'User already exists')
    @api.doc('create_new_user')
    @api.expect(_user_post, validate=True)
    def post(self):
        data = request.json
        res_data, status_code = save_new_user(data=data)
        if status_code == 201:
            return marshal(res_data, _user_get), status_code
        else:
            return res_data, status_code

@api.route('/<userid>')
@api.param('userid', 'The User identifier')
@api.response(404, 'User not found.')
class User(Resource):
    @api.doc('get_a_user')
    @api.marshal_with(_user_get)
    def get(self, userid):
        user = get_a_user(userid)
        if not user:
            api.abort(404, message='User not found.')
        else:
            return user
