from flask_restplus import Namespace, fields


class UserDto:
    userapi = Namespace('users', description='user related operations')
    user_get = userapi.model('users_get', {
        'userid': fields.Integer(required=False, description='user userid'),
        'firstname': fields.String(required=True, description='user firstname'),
        'lastname': fields.String(required=True, description='user lastname'),
        'email': fields.String(required=True, description='user email address'),
        'phone': fields.String(required=True, description='user Identifier'),
        'isadmin': fields.Boolean(required=True, description='If user is admin'),
        'password': fields.String(required=True, description='user password'),
    })
    user_post = userapi.model('users_post', {
        'firstname': fields.String(required=True, description='user firstname'),
        'lastname': fields.String(required=True, description='user lastname'),
        'email': fields.String(required=True, description='user email address'),
        'phone': fields.String(required=True, description='user Identifier'),
        'isadmin': fields.Boolean(required=True, description='If user is admin'),
        'password': fields.String(required=True, description='user password'),
    })

class BookDto:
    bookapi = Namespace('books', description='book related operations')
    book_get = bookapi.model('books_get', {
        'bookid': fields.Integer(required=False, description='book bookid'),
        'bookname': fields.String(required=True, description='book name'),
        'userid': fields.Integer(required=True, description='book userid'),
        'authorid': fields.Integer(required=True, description='book authorid'),
        'genre': fields.String(required=True, description='book genre'),
        'published_date': fields.String(required=True, description='book published date'),
        'isloaned': fields.Boolean(required=False, description='If book is loaned')
    })
    book_put_post = bookapi.model('books_put_post', {
        'bookname': fields.String(required=True, description='book name'),
        'userid': fields.Integer(required=True, description='book userid'),
        'authorid': fields.Integer(required=True, description='book authorid'),
        'genre': fields.String(required=True, description='book genre'),
        'published_date': fields.String(required=True, description='book published date')
    })


class NotesDto:
    notesapi = Namespace('notes', description='notes related operations', path='/books')
    note_get = notesapi.model('notes_get', {
        'noteid': fields.Integer(required=False, description='notes noteid'),
        'comment': fields.String(required=True, description='notes comment'),
        'bookid': fields.Integer(required=True, description='notes bookid')
    })
    note_post = notesapi.model('notes_post', {
        'comment': fields.String(required=True, description='notes comment')
    })


class FavoriteDto:
    favoriteapi = Namespace('favorite', description='favorite related operations')
    favorite_post = favoriteapi.model('favorite', {
        'favoritename': fields.String(required=True, description='favorite name'),
        'userid': fields.Integer(required=True, description='favorite userid'),
    })
    favorite_get = favoriteapi.model('favorite_get', {
        'favoriteid': fields.Integer(required=False, description='favorite id'),
        'favoritename': fields.String(required=True, description='favorite name'),
        'userid': fields.Integer(required=True, description='favorite userid'),
        'datecreated': fields.DateTime(required=False, description='date created'),
    })


class FavoriteListDto:
    favoritelistapi = Namespace('favoritelist', description='favorite related operations')
    favoritelist = favoritelistapi.model('favoritelist', {
        'favoriteid':fields.Integer(required=False, description='favorite id'),
        'bookid':fields.Integer(required=False, description='book id'),
        'dateadded' :fields.DateTime(required=False, description='date created'),
    })


class LoanedBooksDto:
    loanedbooksapi = Namespace('loanedbooks', description='loaned books related operations')
    loanedbooks_get = loanedbooksapi.model('loanedbooks_get', {
        'loanid': fields.Integer(required=False, description='loan loanid'),
        'userid': fields.Integer(required=False, description='user userid'),
        'bookid': fields.Integer(required=False, description='book bookid'),
        'date_issued': fields.String(required=True, description='book loaned date'),
        'date_returned': fields.String(required=False, description='book returned date'),
        'expected_return_date': fields.String(required=True, description='expected book return date'),
    })
    loanedbooks_post = loanedbooksapi.model('loanedbooks_post', {
        'userid': fields.Integer(required=False, description='user userid'),
        'bookid': fields.Integer(required=False, description='book bookid'),
        'date_issued': fields.String(required=True, description='book loaned date'),
        'expected_return_date': fields.String(required=True, description='expected book return date'),
    })

    loanedbooks_put = loanedbooksapi.model('loanedbooks_put', {
        'date_returned': fields.String(required=False, description='book returned date'),
    })


class AuthorsDto:
    authorapi = Namespace('authors', description='author related operations', path='/authors')
    authors_get = authorapi.model('authors_get', {
        'authorid': fields.Integer(required=False, description='author authorid'),
        'firstname': fields.String(required=True, description='author firstname'),
        'lastname': fields.String(required=True, description='author lastname'),
    })
    authors_post = authorapi.model('authors_post', {
        'firstname': fields.String(required=True, description='author firstname'),
        'lastname': fields.String(required=True, description='author lastname'),
    })
