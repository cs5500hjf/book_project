from app.main import db
from app.main.model.notes import Notes
from app.main.model.books import Books
from pprint import pprint

def check_if_book_exists(bookid):
    book = Books.query.filter_by(bookid=bookid).first()
    return book

def save_new_notes(data, bookid):
    new_note = Notes(
        comment=data['comment'],
        bookid=bookid
    )
    save_changes(new_note)
    return new_note, 201


def get_a_note(noteid):
    return Notes.query.filter_by(noteid=noteid).first()


def get_all_notes_book(bookid):
    return Notes.query.filter_by(bookid=bookid)


def get_all_notes():
    return Notes.query.all()

def update_a_note(data, noteid, bookid):
    note = get_a_note(noteid)
    note.comment = data['comment']
    db.session.commit()

    return note, 200

def delete_a_note(noteid):
    note = get_a_note(noteid)
    delete_data(note)
    response_object = {
        'status': 'success',
        'message': 'Successfully deleted Note.'
    }
    return response_object, 200


def save_changes(data):
    db.session.add(data)
    db.session.commit()

def delete_data(data):
    db.session.delete(data)
    db.session.commit()