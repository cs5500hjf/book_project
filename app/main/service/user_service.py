from app.main import db
from app.main.model.user import User
from flask_restplus import marshal


def save_new_user(data):
    user = User.query.filter_by(email=data['email']).first()
    if not user:
        new_user = User(
            firstname=data['firstname'],
            lastname=data['lastname'],
            email=data['email'],
            phone=data['phone'],
            password=data['password'],
            isadmin=data['isadmin']
        )
        save_changes(new_user)
        print(new_user)
        return new_user,  201
    else:
        response_object = {
            'status': 'fail',
            'message': 'User already exists. Please Log in.',
        }
        return response_object, 409


def get_all_users():
    return User.query.all()


def get_a_user(userid):
    return User.query.filter_by(userid=userid).first()


def save_changes(data):
    db.session.add(data)
    db.session.commit()
