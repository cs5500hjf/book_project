from app.main import db
from app.main.model.books import Books


def save_new_book(data):
  book = Books.query.filter_by(bookname=data['bookname']).first()
  if not book:
    new_book = Books(
        bookname=data['bookname'],
        userid=data['userid'],
        authorid=data['authorid'],
        genre=data['genre'],
        published_date = data['published_date']
    )
    save_changes(new_book)
    print(new_book)
    return new_book, 201
  else:
    response_object = {
      'status': 'fail',
      'message': 'Book already exists.',
    }
    return response_object, 409


def update_book_isloaned(bookid):
    book = get_a_book(bookid)
    book.isloaned = True
    db.session.commit()
    return book, 200


def update_book_isreturned(bookid):
    book = get_a_book(bookid)
    book.isloaned = False
    db.session.commit()
    return book, 200


def update_a_book(bookid, data):
    book = get_a_book(bookid)
    book.bookname=data['bookname']
    book.userid=data['userid'],
    book.authorid=data['authorid'],
    book.genre=data['genre'],
    book.published_date = data['published_date']
    db.session.commit()
    return book, 200


def delete_a_book(bookid):
    book = get_a_book(bookid)
    delete_data(book)
    response_object = {
        'status': 'success',
        'message': 'Successfully deleted Book.'
    }
    return response_object, 200


def delete_data(data):
    db.session.delete(data)
    db.session.commit()


def get_all_books():
    return Books.query.all()


def get_all_loaned_books():
  return Books.query.filter_by(isloaned=True).all()


def get_a_book(bookid):
    return Books.query.filter_by(bookid=bookid).first()


def search_a_book(args):
    res = Books.query
    if args['authorid']:
        res = res.filter_by(authorid=args['authorid'])
    if args['genre']:
        res = res.filter_by(genre=args['genre'])
    if args['startdate']:
        res = res.filter(Books.published_date >= args['startdate'])
    if args['enddate']:
        res = res.filter(Books.published_date <= args['enddate'])
    return res.all()


def save_changes(data):
    db.session.add(data)
    db.session.commit()
