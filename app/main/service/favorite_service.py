from app.main import db
from app.main.model.favorites import Favorites
from app.main.model.favoriteslist import FavoritesList
from app.main.model.books import Books
from pprint import pprint
import datetime


def add_a_book_to_favoriteslist(favoriteid, bookid):
    element = FavoritesList.query.filter_by(
        favoriteid=favoriteid, bookid=bookid).first()
    response_object = {}
    status = 404
    if not element:
        new_favoritelist = FavoritesList(
            favoriteid=favoriteid,
            bookid=bookid
        )
        save_changes(new_favoritelist)
        status = 201
        response_object = {
            'status': 'success',
            'message': 'Successfully added book to favoritelist.'
        }
    else:
        response_object = {
            'message': 'book already exists in this list',
            'status':'fail'
        }
    return response_object, status


def delete_a_book_from_favoriteslist(favoriteid, bookid):
    element = FavoritesList.query.filter_by(
        favoriteid=favoriteid, bookid=bookid).first()
    response_object = {}
    status = 404
    if not element:
        response_object = {
            'status': 'fail',
            'message': 'book does not exist'
        }
    else:
        delete_data(element)
        response_object = {
            'status': 'success',
            'message': 'Successfully deleted book from list.'
        }
        status = 200
        
    return response_object, status


def create_a_favorites(data):
    new_favorite = Favorites(
        name=data['favoritename'],
        userid=data['userid'],
    )
    save_changes(new_favorite)
    return new_favorite, 201

def get_favorites(favoriteid):
    favorite = Favorites.query.filter_by(favoriteid=favoriteid).first()
    if not favorite:
        response_object = {
            'status': 'fail',
            'message': 'favorite list not exist.',
        }
        return response_object, 404
    else:
        return favorite, 200


def delete_a_favorites(favoriteid):
    favorite = Favorites.query.filter_by(favoriteid=favoriteid).first()

    if not favorite:
        
        return {
            'status': 'fail',
            'message': 'favorite list not exist.',
        }, 404
    else:
        favlistbooks = FavoritesList.query.filter_by(favoriteid=favoriteid).all()
        for book in favlistbooks:
            delete_data(book)
        delete_data(favorite)
        return {
            'status': 'success',
            'message': 'favorite list deleted.'
        }, 200

def get_books_by_favoriteid(favoriteid):
    favlistbooks = FavoritesList.query.filter_by(favoriteid=favoriteid).all()
    if not favlistbooks:
        response_object = {
            'status': 'fail',
            'message': 'no books exists in list',
        }
        return response_object, 404
    else:
        return favlistbooks, 200

def delete_data(data):
    db.session.delete(data)
    db.session.commit()


def save_changes(data):
    db.session.add(data)
    db.session.commit()
