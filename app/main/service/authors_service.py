from .. import db
from app.main.model.author import Author
from sqlalchemy import and_

def save_new_author(data):
  author = Author.query.filter(and_(Author.firstname == data['firstname'], Author.lastname == data['lastname'])).first()
  if not author:
    new_author = Author(
      firstname=data['firstname'],
      lastname=data['lastname'],
    )
    save_changes(new_author)

    return new_author, 201
  else:
    response_object = {
      'status': 'fail',
      'message': 'Author already exists.',
    }
    return response_object, 409


def update_author(authorid, data):
    author = get_author(authorid)
    author.firstname = data['firstname']
    author.lastname = data['lastname']
    db.session.commit()
    return author, 200


def get_all_authors():
    return Author.query.all()

def get_author(authorid):
    return Author.query.filter_by(authorid=authorid).first()

def get_authorid(firstname, lastname):
    author = Author.query.filter(and_(firstname=firstname, lastname=lastname))
    return author.authorid

def delete_author(authorid):
    author = get_author(authorid)
    delete_data(author)
    response_object = {
        'status': 'success',
        'message': 'Successfully deleted Author.'
    }
    return response_object, 200

def save_changes(data):
    db.session.add(data)
    db.session.commit()

def delete_data(data):
    db.session.delete(data)
    db.session.commit()
