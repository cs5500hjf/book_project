from app.main import db
from app.main import mail
from app.main.model.loanedbooks import LoanedBooks
from app.main.model.user import User
from sqlalchemy import and_
from datetime import datetime
from flask_mail import Message, Mail
from smtplib import SMTP
import os
import logging


def get_all_loan_records():
  return LoanedBooks.query.all()

def delete_book_loans(bookid):
  loans = LoanedBooks.query.filter_by(bookid=bookid).all()

  for loan in loans:
   delete_data(loan)

  response_object = {
      'status': 'success',
      'message': 'Successfully deleted Loan.'
  }
  return response_object, 200

def delete_data(data):
    db.session.delete(data)
    db.session.commit()


def save_new_loaned_book(data):
  new_loaned_book = LoanedBooks(
    bookid=data['bookid'],
    userid=data['userid'],
    date_issued=data['date_issued'],
    expected_return_date=data['expected_return_date']
  )
  save_changes(new_loaned_book)
  print(new_loaned_book)
  return new_loaned_book, 200


def send_reminder_to_user(loanid):
  loan = LoanedBooks.query.filter_by(loanid=loanid).first()
  if(loan.date_returned.year != 1900):
    return 'Book returned already!'
  userid = loan.userid
  user = User.query.filter_by(userid=userid).first()
  email = user.email
  returndate = loan.expected_return_date
  returnby = returndate.strftime('%m/%d/%Y')

  email_content = "Return Book Reminder"
  email_body = "Hello, This is a reminder to return your book by {}. Thank you!".format(returnby)
  msg = Message(email_content, recipients=[email], sender="bookscatalogproject@gmail.com", body=email_body)

  mail.send(msg)
  return 'Sent!'

def get_loans_to_be_returned():
  today = datetime.now()
  day = (today.day)+2
  year = today.year
  month = today.month

  loans = LoanedBooks.query.filter(and_(LoanedBooks.expected_return_date == datetime(year, month, day),
                                        LoanedBooks.date_returned == datetime(1900, 9, 9))).all()

  return loans

def get_email_of_users():
  loans = get_loans_to_be_returned()
  users_email = []
  for loan in loans:
    userid = loan.userid
    user = User.query.filter_by(userid=userid).first()
    email_id= user.email
    users_email.append(email_id)

  return users_email

def remind_all_users():
  email_users = get_email_of_users()
  if not email_users:
    return "No pending book returns!"

  email_content = "Return Book Reminder"

  msg = Message(email_content, recipients=email_users, sender="bookscatalogproject@gmail.com",
                  body="Hello, This is a reminder to return your " \
                       "books within the next 2 days, Thank you!")

  mail.send(msg)

  return 'Sent!'

def set_the_return_date(data, loanid):
  loan = LoanedBooks.query.filter_by(loanid=loanid).first()
  loan.date_returned = data['date_returned']
  db.session.commit()
  return loan, 200


def get_books_loanedby_user(userid):
  booksloaned = LoanedBooks.query.filter(LoanedBooks.userid==userid).all()
  print(booksloaned)
  return booksloaned


def save_changes(data):
    db.session.add(data)
    db.session.commit()
