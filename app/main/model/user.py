from .. import db

class User(db.Model):
    """ User Model for storing user related details """
    __tablename__ = "users"

    userid = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.String(255), unique=True, nullable=False)
    phone = db.Column(db.String(255), unique=True, nullable=False)
    isadmin = db.Column(db.Boolean, nullable=False)
    password = db.Column(db.String(100), unique=True)
    firstname = db.Column(db.String(50), nullable=False)
    lastname = db.Column(db.String(50), nullable=False)


    def __init__(self, firstname, lastname, password, email, phone, isadmin=False):
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.phone = phone
        self.isadmin = isadmin
        self.password = password

    # def __repr__(self):
    #     return self
