from .. import db

class Notes(db.Model):
    """ Notes table for notes details """
    __tablename__ = "notes"

    noteid = db.Column(db.Integer, primary_key=True, nullable=False)
    comment = db.Column(db.String(255), nullable=False)
    bookid = db.Column(db.Integer, db.ForeignKey('books.bookid'), nullable=False)

    def __init__(self, comment, bookid):
        self.comment = comment
        self.bookid = bookid

    # def __repr__(self):
    #     return '<node {}>'.format(self.noteid)
