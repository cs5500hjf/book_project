from .. import db
from datetime import datetime

# class Genre(db.Enum):
#     SCI_FI = "sci-fi"
#     THRILLER = "thriller"
#     ACTION = "action"
#     ROMANCE = "ROMANCE"
#     COMEDY = "COMEDY"


class Books(db.Model):
    """ Books model for storing books details """
    __tablename__ = "books"

    bookid = db.Column(db.Integer, primary_key=True, nullable=False)
    userid = db.Column(db.Integer, db.ForeignKey('users.userid'), nullable=False)
    bookname = db.Column(db.String(255), nullable=False)
    authorid = db.Column(db.Integer, db.ForeignKey('authors.authorid'), nullable=False)
    genre = db.Column(db.Enum("sci-fi", "thriller", "action", "romance", "comedy", name = "genre_enum"))
    published_date = db.Column(db.DateTime, default=datetime.now())
    isloaned = db.Column(db.Boolean, default=False ,nullable=True)

    def __init__(self, bookname, userid, authorid, genre, published_date):
        self.bookname = bookname
        self.userid = userid
        self.authorid = authorid
        self.genre = genre
        self.published_date = published_date

    def __repr__(self):
        return '<bookid {}>'.format(self.bookid)
