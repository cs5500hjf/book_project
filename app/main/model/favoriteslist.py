from .. import db
from datetime import datetime

class FavoritesList(db.Model):
    """ FavoritesList table for favorites_list details """
    __tablename__ = "favorites_list"

    favoriteid = db.Column(db.Integer, db.ForeignKey('favorites.favoriteid'), nullable=False, primary_key=True)
    bookid = db.Column(db.Integer, db.ForeignKey('books.bookid'), nullable=False, primary_key=True)
    dateadded= db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, favoriteid, bookid):
        self.favoriteid = favoriteid
        self.bookid = bookid

    def __repr__(self):
        return '<favoritesid {}>'.format(self.favoriteid)
