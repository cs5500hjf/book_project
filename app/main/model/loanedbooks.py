from .. import db
from datetime import datetime

class LoanedBooks(db.Model):
    """ LoanedBooks model for storing details of books which are loaned """
    __tablename__ = "loanedbooks"

    loanid = db.Column(db.Integer, primary_key=True, nullable=False)
    bookid = db.Column(db.Integer, db.ForeignKey('books.bookid'), nullable=False)
    userid = db.Column(db.Integer, db.ForeignKey('users.userid'), nullable=False)
    date_issued = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    date_returned = db.Column(db.DateTime, default=datetime.date(datetime(1900, 9, 9, 0, 0)), nullable=True)
    expected_return_date = db.Column(db.DateTime, nullable=False)

    def __init__(self, bookid, userid, date_issued, expected_return_date):
        self.bookid = bookid
        self.userid = userid
        self.date_issued = date_issued
        self.expected_return_date = expected_return_date

    def __repr__(self):
        return '<loanid {}>'.format(self.loanid)
