from .. import db

class Author(db.Model):
    """ Author table for author details """
    __tablename__ = "authors"

    authorid = db.Column(db.Integer, primary_key=True, nullable=False)
    firstname = db.Column(db.String(50), nullable=False)
    lastname = db.Column(db.String(50), nullable=False)

    def __init__(self, firstname, lastname):
        self.firstname = firstname
        self.lastname = lastname

    def __repr__(self):
        return '<authorid {}>'.format(self.authorid)
