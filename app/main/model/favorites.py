from .. import db
from datetime import datetime

class Favorites(db.Model):
    """ Favorites table for favorites details """
    __tablename__ = "favorites"

    favoriteid = db.Column(db.Integer, primary_key=True, nullable=False)
    favoritename = db.Column(db.String(50), nullable=False)
    userid = db.Column(db.Integer, db.ForeignKey('users.userid'), nullable=False)
    datecreated= db.Column(db.DateTime, default=datetime.utcnow)

    def __init__(self, name, userid):
        
        self.favoritename = name
        self.userid = userid
        

    def __repr__(self):
        return '<favoritesid {}>'.format(self.favoriteid)
