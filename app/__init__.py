# app/__init__.py
import os
from flask_restplus import Api
from flask import Blueprint, url_for

from .main.controller.user_controller import api as user_ns
from .main.controller.book_controller import bookapi as book_ns
from .main.controller.notes_controller import api as notes_ns

from .main.controller.author_controller import api as author_ns
from .main.controller.loanedbooks_controller import api as loanedbooks_ns

from .main.controller.favorite_controller import api as favorite_ns


env = os.getenv('APP_ENV') or 'dev'

if env == 'prod' or env == 'test':
    @property
    def specs_url(self):
        """Monkey patch for HTTPS"""
        return url_for(self.endpoint('specs'), _external=True, _scheme='https')

    Api.specs_url = specs_url

blueprint = Blueprint('api', __name__)

api = Api(blueprint)

api.add_namespace(user_ns)
api.add_namespace(book_ns)
api.add_namespace(notes_ns)
api.add_namespace(loanedbooks_ns)
api.add_namespace(author_ns)
api.add_namespace(favorite_ns)

