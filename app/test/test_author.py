import pytest
import json
import bravado
from bravado.client import SwaggerClient
import os

from manage import app

@pytest.fixture()
def test_client():

    client = app.test_client()

    ctx = app.app_context()
    ctx.push()

    yield client

    ctx.pop()

def test_authors_bravado():
    client = SwaggerClient.from_url(os.environ.get('SWAGGER_URL', 'http://localhost:5000/swagger.json'))
    Author = client.get_model('authors_post')

    author = Author(firstname="Prakash", lastname="S")

    author = json.loads(client.authors.add_new_author(payload=author).response().incoming_response.text)
    assert author['authorid'] != None
    assert author['firstname'] == 'Prakash'
    assert author['lastname'] == 'S'

    new_author = Author(firstname="John", lastname="Doe")
    new_author = json.loads(client.authors.add_new_author(payload=new_author).response().incoming_response.text)
    assert new_author['authorid'] != None

    authors = json.loads(client.authors.get_all_authors().response().incoming_response.text)
    assert len(authors) >= 2

    get_author = json.loads(client.authors.get_author_by_this_id(authorid=str(new_author['authorid'])).response().incoming_response.text)
    assert get_author['firstname'] == 'John'
    assert get_author['lastname'] == 'Doe'

    update_author = Author(firstname="Joe", lastname="Mac")
    update_author = json.loads(client.authors.update_an_author(payload=update_author, authorid=str(new_author['authorid'])).response().incoming_response.text)
    assert  update_author['firstname'] == "Joe"
    assert update_author['authorid'] == new_author['authorid']

    del_response = client.authors.delete_an_author(authorid=str(new_author['authorid'])).response()
    assert del_response.metadata.status_code == 200

    del_response = client.authors.delete_an_author(authorid=str(author['authorid'])).response()
    assert del_response.metadata.status_code == 200

