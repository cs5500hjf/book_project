import pytest
import json
from bravado.client import SwaggerClient
import os

from manage import app

@pytest.fixture()
def test_client():

    client = app.test_client()

    ctx = app.app_context()
    ctx.push()

    yield client

    ctx.pop()


def test_notes_bravado():
    client = SwaggerClient.from_url(os.environ.get('SWAGGER_URL', 'http://localhost:5000/swagger.json'))
    Note = client.get_model('notes_post')

    note = Note(comment="Good Book")

    note = json.loads(client.notes.create_new_note(payload=note, bookid=str(1)).response().incoming_response.text)
    assert note['noteid'] != None
    assert note['comment'] == "Good Book"
    assert note['bookid'] == 1

    notes = json.loads(client.notes.list_of_all_notes(bookid=str(1)).response().incoming_response.text)
    assert len(notes) > 0

    updated_note = Note(comment="Bad Book")
    updated_note = json.loads(client.notes.update_a_note(payload=updated_note, bookid=str(1), noteid=str(note['noteid'])).response().incoming_response.text)

    assert updated_note['noteid'] == note['noteid']
    assert updated_note['comment'] == "Bad Book"
    assert updated_note['bookid'] == 1

    delete_response= json.loads(client.notes.delete_a_note(noteid=str(updated_note['noteid']), bookid=str(1)).response().incoming_response.text)
    assert delete_response['status'] == "success"
