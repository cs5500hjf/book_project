import pytest
import json
from bravado.client import SwaggerClient
import os

from manage import app

@pytest.fixture()
def test_client():

    client = app.test_client()

    ctx = app.app_context()
    ctx.push()

    yield client

    ctx.pop()


def test_users_bravado():
    client = SwaggerClient.from_url(os.environ.get('SWAGGER_URL', 'http://localhost:5000/swagger.json'))
    User = client.get_model('users_post')

    user = User(email="foo@bar2.com", firstname="Grand", lastname="Poobah",
                isadmin=True, password="totallyinsecure2", phone="1-800-I-AM-ROOT32")

    user = json.loads(client.users.create_new_user(payload=user).response().incoming_response.text)
    assert user['userid'] != None
    assert user['email'] == 'foo@bar2.com'
    assert user['firstname'] == 'Grand'

    try:
        client.users.create_new_user(payload=user).result()
    except Exception as e:
        raised_exception = e

    assert raised_exception is not None
    response = raised_exception.response
    assert response.status_code == 409

    user = json.loads(client.users.get_a_user(userid=str(user['userid'])).response().incoming_response.text)
    assert user['userid'] != None
    assert user['email'] == 'foo@bar2.com'
    assert user['firstname'] == 'Grand'

    try:
        client.users.get_a_user(userid='9999').result()
    except Exception as e:
        not_found_exception = e
    assert not_found_exception is not None
    response = not_found_exception.response
    assert response.status_code == 404

    users = json.loads(client.users.list_of_all_users().response().incoming_response.text)
    assert len(users) > 0

    return user


