import pytest
import json
import bravado
from bravado.client import SwaggerClient
import os
import datetime

from manage import app

@pytest.fixture()
def test_client():

    client = app.test_client()

    ctx = app.app_context()
    ctx.push()

    yield client

    ctx.pop()


def test_books_bravado():
    client = SwaggerClient.from_url(os.environ.get('SWAGGER_URL', 'http://localhost:5000/swagger.json'))

    Author = client.get_model('authors_post')
    """create an author"""

    author = Author(firstname="ruskin", lastname="bond")

    author = json.loads(client.authors.add_new_author(payload=author).response().incoming_response.text)
    assert author['authorid'] != None
    assert author['firstname'] == 'ruskin'
    assert author['lastname'] == 'bond'

    authorid = author['authorid']

    """create user"""

    User = client.get_model('users_post')

    user = User(email="foo@bar345.com", firstname="hello", lastname="world",
                isadmin=True, password="hello123", phone="1-800-I-AM-ROOT33")

    user = json.loads(client.users.create_new_user(payload=user).response().incoming_response.text)
    assert user['userid'] != None
    assert user['email'] == 'foo@bar345.com'
    assert user['firstname'] == 'hello'

    Book = client.get_model('books_put_post')

    """create book"""
    book = Book(bookname="rusty", userid=user['userid'], authorid=authorid, genre="sci-fi", published_date=str(datetime.datetime(2002,7,1,0,0,0)))

    book = json.loads(client.books.add_new_book(payload=book).response().incoming_response.text)
    d = datetime.datetime(2002,7,1,0,0,0)
    assert book['bookid'] != None
    assert book['bookname'] == 'rusty'
    assert book['userid'] == user['userid']
    assert book['authorid'] == authorid
    assert book['genre'] == 'sci-fi'
    assert book['published_date'] == str(d)

    """test get_a_book"""
    get_book = json.loads(client.books.get_a_book(bookid=str(book['bookid'])).response().incoming_response.text)
    assert get_book['bookid'] == book['bookid']

    """create book2"""
    book2 = Book(bookname="harry potter", userid=user['userid'], authorid=authorid, genre="sci-fi", published_date=str(datetime.datetime(2000,10,2,0,0,0)))

    book2 = json.loads(client.books.add_new_book(payload=book2).response().incoming_response.text)
    date2 = datetime.datetime(2000,10,2,0,0,0)
    assert book2['bookid'] != None
    assert book2['bookname'] == 'harry potter'
    assert book2['userid'] == user['userid']
    assert book2['authorid'] == authorid
    assert book2['genre'] == 'sci-fi'
    assert book2['published_date'] == str(date2)

    """test all_books"""
    book_list = json.loads(client.books.all_books().response().incoming_response.text)
    assert len(book_list) >= 2

    """update book2"""
    book_update = Book(bookname="harry potter 2", userid=user['userid'], authorid=authorid, genre="action", published_date=str(datetime.datetime(2002,7,1,0,0,0)))
    book_update = json.loads(client.books.update_a_book(payload=book_update, bookid=str(book2['bookid'])).response().incoming_response.text)
    assert book_update['bookname'] == 'harry potter 2'
    assert book_update['userid'] == user['userid']
    assert book_update['authorid'] == authorid
    assert book_update['genre'] == 'action'
    assert book_update['published_date'] == str(datetime.datetime(2002,7,1,0,0,0))
    assert book_update['bookid'] == book2['bookid']

    """create book3"""
    book3 = Book(bookname="sherlock holmes", userid=user['userid'], authorid=authorid, genre="sci-fi", published_date=str(datetime.datetime(2002,7,1,0,0,0)))

    book3 = json.loads(client.books.add_new_book(payload=book3).response().incoming_response.text)
    assert book3['bookid'] != None
    assert book3['bookname'] == 'sherlock holmes'
    assert book3['userid'] == user['userid']
    assert book3['authorid'] == authorid
    assert book3['genre'] == 'sci-fi'
    assert book3['published_date'] == str(d)


    """create two users(not admin) to loan books to"""
    """create user1"""
    user1 = User(email="pd@gmail.com", firstname="prakriti", lastname="dave",
                isadmin=False, password="pd", phone="1-700-I-AM-ROOT30")

    user1 = json.loads(client.users.create_new_user(payload=user1).response().incoming_response.text)
    assert user1['userid'] != None
    assert user1['email'] == 'pd@gmail.com'
    assert user1['firstname'] == 'prakriti'
    assert user1['lastname'] == 'dave'

    """create user2"""
    user2 = User(email="ben@gmail.com", firstname="ben", lastname="john",
                isadmin=False, password="ben", phone="1-700-I-AM-ROOT20")

    user2 = json.loads(client.users.create_new_user(payload=user2).response().incoming_response.text)
    assert user2['userid'] != None
    assert user2['email'] == 'ben@gmail.com'
    assert user2['firstname'] == 'ben'
    assert user2['lastname'] == 'john'

    """loan books to user1 and user2"""

    loanedbooks = client.get_model('loanedbooks_post')

    todaydate = datetime.date.today()
    return_date1 = datetime.datetime(todaydate.year,todaydate.month,todaydate.day+7,0,0,0)

    """create loan1, loaned book to user1"""
    loan1 = loanedbooks(userid=user1['userid'], bookid=book['bookid'], date_issued=str(todaydate)
                        , expected_return_date=str(return_date1))

    loan1 = json.loads(client.books.loan_this_book_to_the_user(bookid=str(book['bookid']), userid=str(user1['userid']),
                                                                     payload=loan1).response().incoming_response.text)
    assert loan1['loanid'] != None
    assert loan1['userid'] == user1['userid']
    assert loan1['bookid'] == book['bookid']

    """create loan2, loaned book_update to user2"""
    loan2 = loanedbooks(userid=user2['userid'], bookid=book_update['bookid'], date_issued=str(todaydate)
                        , expected_return_date=str(return_date1))

    loan2 = json.loads(client.books.loan_this_book_to_the_user(bookid=str(book_update['bookid']), userid=str(user2['userid']),
                                                                     payload=loan2).response().incoming_response.text)
    assert loan2['loanid'] != None
    assert loan2['userid'] == user2['userid']
    assert loan2['bookid'] == book_update['bookid']

    """create loan3, loaned book3 to user1"""
    loan3 = loanedbooks(userid=user1['userid'], bookid=book3['bookid'], date_issued=str(todaydate)
                        , expected_return_date=str(return_date1))

    loan3 = json.loads(client.books.loan_this_book_to_the_user(bookid=str(book3['bookid']), userid=str(user1['userid']),
                                                                     payload=loan3).response().incoming_response.text)
    assert loan3['loanid'] != None
    assert loan3['userid'] == user1['userid']
    assert loan3['bookid'] == book3['bookid']

    """test get_all_loaned_books"""
    loaned_books_list = json.loads(client.books.get_all_loaned_books().response().incoming_response.text)
    assert len(loaned_books_list) >= 2

    """test get all loan records"""

    loans_list = json.loads(client.loanedbooks.view_all_loan_records().response().incoming_response.text)
    assert len(loans_list) >= 2

    """get list loan records by user"""
    user_loans_list = json.loads(client.loanedbooks.get_all_loan_records_by_user(userid=str(user1['userid'])).response().incoming_response.text)
    assert len(user_loans_list) >= 2

    # loan2['date_returned'] = str(datetime.datetime(2018,7,20,0,0,0))
    # loan_book_returned = json.loads(client.books.book_is_returned_by_user(bookid=str(book_update['bookid']), loanid=str(loan2['loanid']),
    #                                                                  payload=loan2).response().incoming_response.text)
    # assert loan_book_returned['loanid'] == loan2['loanid']
    # assert loan_book_returned['userid'] == loan2['userid']
    # assert loan_book_returned['bookid'] == book_update['bookid']
    # assert loan_book_returned['date_returned'] != str(datetime.datetime(1900, 9, 9, 0, 0))
