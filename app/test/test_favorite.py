from bravado.client import SwaggerClient
import json
import pytest
import os

from manage import app

@pytest.fixture()
def test_client():

    client = app.test_client()

    ctx = app.app_context()
    ctx.push()

    yield client

    ctx.pop()

def test_favorite_bravado():
    client = SwaggerClient.from_url(os.environ.get('SWAGGER_URL', 'http://localhost:5000/swagger.json'))
    Favorite = client.get_model('favorite')

    favorite = Favorite(favoritename="jun's fav", userid=1)

    myFavorite = json.loads(client.favorite.create_a_favorite(
        payload=favorite).response().incoming_response.text)

    assert(myFavorite["favoritename"] == "jun's fav")

    getFavorite = json.loads(client.favorite.get_favorites_by_id(
        favoriteid=str(myFavorite["favoriteid"])).response().incoming_response.text)
    # print(getFavorite)
    assert(getFavorite["favoritename"] == "jun's fav")

    deleteFavorite = json.loads(client.favorite.delete_a_favorites(favoriteid=str(myFavorite["favoriteid"]))
    .response().incoming_response.text)
    assert(deleteFavorite["status"] == "success")

def test_favoritelist_bravodo():
    client = SwaggerClient.from_url(os.environ.get('SWAGGER_URL', 'http://localhost:5000/swagger.json'))
    Favorite = client.get_model('favorite')
    favorite = Favorite(favoritename="jun's fav", userid=1)
    myFavorite = json.loads(client.favorite.create_a_favorite(
        payload=favorite).response().incoming_response.text)
    addBookResponse = json.loads(client.favorite.add_a_book_to_favoriteslist(
        favoriteid=str(myFavorite["favoriteid"]), bookid=str(1)).response().incoming_response.text)
    assert(addBookResponse["status"] == "success")
    deleteBookResponse = json.loads(client.favorite.delete_a_book_from_favoriteslist(
        favoriteid=str(myFavorite["favoriteid"]), bookid=str(1)).response().incoming_response.text)
    # print(deleteBookResponse)
    assert(addBookResponse["status"] == "success")

