"""create users table

Revision ID: c355af7e24a4
Revises: 405d3b9aec49
Create Date: 2018-06-09 01:41:46.293580

"""

# revision identifiers, used by Alembic.
revision = 'c355af7e24a4'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('users',
    sa.Column('userid', sa.Integer(), nullable=False),
    sa.Column('email', sa.String(length=255), nullable=False),
    sa.Column('phone', sa.String(length=255), nullable=False),
    sa.Column('isadmin', sa.Boolean(), nullable=False),
    sa.Column('password', sa.String(length=100), nullable=True),
    sa.Column('firstname', sa.String(length=50), nullable=False),
    sa.Column('lastname', sa.String(length=50), nullable=False),
    sa.PrimaryKeyConstraint('userid'),
    sa.UniqueConstraint('email'),
    sa.UniqueConstraint('password'),
    sa.UniqueConstraint('phone')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('users')
    # ### end Alembic commands ###
